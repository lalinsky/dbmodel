DEPENDPATH += $$PWD

RESOURCES = ../dbmodel.qrc

# Windows RC (for embedded icon)
RC_FILE = ../dbmodel.rc

SOURCES = \
	connector.cpp \
	hub.cpp \
	boxsidehub.cpp \
	main.cpp \
	mainwindow.cpp \
	diagramview.cpp \
	diagramdocument.cpp \
	line.cpp \
	diagramitem.cpp \
	diagramitemfactory.cpp \
	diagramitemproperties.cpp \
	diagramobject.cpp \
	identifiableobject.cpp \
	commands.cpp

HEADERS = \
	connector.h \
	hub.h \
	boxsidehub.h \
	mainwindow.h \
	diagramview.h \
	diagramdocument.h \
	line.h \
	diagramitem.h \
	diagramitemfactory.h \
	diagramitemproperties.h \
	diagramobject.h \
	identifiableobject.h \
	commands.h

TRANSLATIONS = \
	../translations/dbmodel_en.ts \
	../translations/dbmodel_de.ts \
	../translations/dbmodel_ru.ts \
	../translations/dbmodel_sk.ts \ 
	../translations/dbmodel_fr.ts

