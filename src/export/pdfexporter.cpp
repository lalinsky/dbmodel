// Copyright (C) 2009  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#include <QPrinter>
#include <QPainter>
#include <math.h>
#include "diagramdocument.h"
#include "pdfexporter.h"

QString
PdfExporter::name() const
{
    return "Portable Document Format";
}

QString
PdfExporter::extension() const
{
    return ".pdf";
}

void
PdfExporter::exportToFile(const QString &fileName, DiagramDocument *document)
{
    QRectF boundingRect = document->itemsBoundingRect().adjusted(-2, -2, 2, 2);
    QSize size(int(ceil(boundingRect.width())), int(ceil(boundingRect.height())));

    QPrinter printer;
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(fileName);
    QPainter painter(&printer);
    painter.setRenderHints(QPainter::HighQualityAntialiasing | QPainter::Antialiasing);

    document->print(&painter, QRectF(), boundingRect);
}
