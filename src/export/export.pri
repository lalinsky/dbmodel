DEPENDPATH += $$PWD

SOURCES += \
    exporter.cpp \
    exporterlist.cpp \
    pngexporter.cpp \
    pdfexporter.cpp \
    svgexporter.cpp

HEADERS += \
    exporter.h \
    exporterlist.h \
    pngexporter.h \
    pdfexporter.h \
    svgexporter.h
