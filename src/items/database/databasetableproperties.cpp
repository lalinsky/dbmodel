// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#include <QGridLayout>
#include <QDebug>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include "commands.h"
#include "columnlist.h"
#include "columnlistview.h"
#include "databasetable.h"
#include "databasetableproperties.h"
#include "diagramdocument.h"
#include "utils/colorpicker/qtcolorpicker.h"

class DatabaseTableProperties::PrivateData
{
public:
	PrivateData()
		{}
	
	QLineEdit *nameEdit;
	ColumnListView *columnListView;
	QPushButton *addColumnButton;
	QPushButton *removeColumnButton;
	QPushButton *moveColumnUpButton;
	QPushButton *moveColumnDownButton;
	QtColorPicker *colorPicker;
	bool ignoreUpdates;
};

DatabaseTableProperties::DatabaseTableProperties(QWidget *parent)
	: DiagramItemProperties(parent), d(new PrivateData)
{
	d->ignoreUpdates = false;
	addPage(tr("&Table"), createTablePage());
	addPage(tr("&Columns"), createColumnsPage());
}

DatabaseTable *
DatabaseTableProperties::currentTable()
{
	return static_cast<DatabaseTable *>(currentItem());
}

QWidget *
DatabaseTableProperties::createTablePage()
{
	QWidget *page = new QWidget(this);
	QGridLayout *layout = new QGridLayout(page);

	d->nameEdit = new QLineEdit(page);
	connect(d->nameEdit, SIGNAL(textEdited(const QString &)), SLOT(setTableName(const QString &)));
	layout->addWidget(new QLabel(tr("Name:"), page), 0, 0);
	layout->addWidget(d->nameEdit, 0, 1);

	d->colorPicker = new QtColorPicker(page);
	d->colorPicker->insertColor(Qt::white, tr("White"));
	d->colorPicker->insertColor(QColor("#AAAAAA"), tr("Gray"));
	d->colorPicker->insertColor(QColor("#777777"), tr("Gray"));
	d->colorPicker->insertColor(QColor("#333333"), tr("Gray"));
	d->colorPicker->insertColor(QColor("#FFCCCC"), tr("Red"));
	d->colorPicker->insertColor(QColor("#CC9900"), tr("Brown"));
	d->colorPicker->insertColor(QColor("#FFCCFF"), tr("Pink"));
	d->colorPicker->insertColor(QColor("#FFFFCC"), tr("Yellow"));
	d->colorPicker->insertColor(QColor("#CCFFCC"), tr("Green"));
	d->colorPicker->insertColor(QColor("#CCECFF"), tr("Blue"));
	connect(d->colorPicker, SIGNAL(colorChanged(const QColor &)), SLOT(setTableColor(const QColor &)));
	layout->addWidget(new QLabel(tr("Color:"), page), 1, 0);
	QHBoxLayout *hbox = new QHBoxLayout();
	hbox->addWidget(d->colorPicker);
	hbox->addStretch();
	layout->addLayout(hbox, 1, 1);
	layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding), 2, 0, 2, 2);

	return page;
}

QWidget *
DatabaseTableProperties::createColumnsPage()
{
	QWidget *page = new QWidget(this);
	QGridLayout *layout = new QGridLayout(page);

	d->columnListView = new ColumnListView(page);
	layout->addWidget(d->columnListView, 0, 0, 5, 1);

	connect(d->columnListView->selectionModel(),
		SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
		SLOT(updateColumnSelection()));

	d->addColumnButton = new QPushButton(tr("&Add"), page);
	d->removeColumnButton = new QPushButton(tr("&Remove"), page);
	d->moveColumnUpButton = new QPushButton(tr("Move &Up"), page);
	d->moveColumnDownButton = new QPushButton(tr("Move &Down"), page);
	layout->addWidget(d->addColumnButton, 0, 1);
	layout->addWidget(d->removeColumnButton, 1, 1);
	layout->addWidget(d->moveColumnUpButton, 2, 1);
	layout->addWidget(d->moveColumnDownButton, 3, 1);
 	layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding), 4, 1, 1, 1);

	connect(d->addColumnButton, SIGNAL(clicked()), d->columnListView, SLOT(addColumn()));
	connect(d->removeColumnButton, SIGNAL(clicked()), d->columnListView, SLOT(removeColumn()));
	connect(d->moveColumnUpButton, SIGNAL(clicked()), d->columnListView, SLOT(moveColumnUp()));
	connect(d->moveColumnDownButton, SIGNAL(clicked()), d->columnListView, SLOT(moveColumnDown()));

	return page;
}

void
DatabaseTableProperties::switchCurrentItem(DiagramItem *oldItem, DiagramItem *)
{
	if (oldItem)
		disconnect(oldItem, 0, this, 0);
	DatabaseTable *table = currentTable();
	if (table) {
		d->nameEdit->setText(table->name());
		d->colorPicker->blockSignals(true);
		d->colorPicker->setCurrentColor(table->color());
		d->colorPicker->blockSignals(false);
		d->columnListView->setColumnList(table->columnList());
		connect(table, SIGNAL(propertyChanged(const QString &, const QVariant &)), SLOT(updateProperty(const QString &, const QVariant &)));
	}
	else {
		d->nameEdit->clear();
		d->columnListView->setColumnList(NULL);
	}
	updateColumnSelection();
}

void
DatabaseTableProperties::updateProperty(const QString &name, const QVariant &value)
{
	if (d->ignoreUpdates)
		return;
	if (name == "name") {
		d->nameEdit->setText(value.toString());
	}
	else if (name == "color") {
		d->colorPicker->setCurrentColor(qVariantValue<QColor>(value));
	}
}

void
DatabaseTableProperties::setTableName(const QString &name)
{
	DatabaseTable *table = currentTable();
	d->ignoreUpdates = true;
	table->document()->undoStack()->push(new SetObjectPropertyCommand(table, "name", name));
	d->ignoreUpdates = false;
}

void
DatabaseTableProperties::setTableColor(const QColor &color)
{
	DatabaseTable *table = currentTable();
	d->ignoreUpdates = true;
	table->document()->undoStack()->push(new SetObjectPropertyCommand(table, "color", color));
	d->ignoreUpdates = false;
}

void
DatabaseTableProperties::updateColumnSelection()
{
	QList<int> columns = d->columnListView->selectedColumns();
	if (columns.isEmpty()) {
		d->removeColumnButton->setEnabled(false);
		d->moveColumnUpButton->setEnabled(false);
		d->moveColumnDownButton->setEnabled(false);
	}
	else {
		int index = columns[0];
		int lastIndex = currentTable()->columnList()->columnCount() - 1;
		d->removeColumnButton->setEnabled(index >= 0 && index <= lastIndex);
		d->moveColumnUpButton->setEnabled(index > 0);
		d->moveColumnDownButton->setEnabled(index < lastIndex);
	}
}
