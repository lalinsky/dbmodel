// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#include <QDebug>
#include "databasecommands.h"
#include "diagramdocument.h"
#include "line.h"
#include "diagramitem.h"
#include "databasetable.h"
#include "column.h"


AddColumnCommand::AddColumnCommand(ColumnList *columnList, Column *column, QUndoCommand *parent)
	: QUndoCommand(parent), m_columnList(columnList), m_column(column)
{
	if (!m_column) {
		m_column = new Column();
		m_column->createId();
	}
	m_index = columnList->columnCount();
}

AddColumnCommand::~AddColumnCommand()
{
	if (m_column)
		delete m_column;
}

void
AddColumnCommand::redo()
{
	m_columnList->insertColumn(m_index, m_column);
	m_column = 0;
}

void
AddColumnCommand::undo()
{
	m_column = m_columnList->removeColumn(m_index);
}


RemoveColumnCommand::RemoveColumnCommand(ColumnList *columnList, int index, QUndoCommand *parent)
	: QUndoCommand(parent), m_columnList(columnList), m_index(index), m_column(0)
{
}

RemoveColumnCommand::~RemoveColumnCommand()
{
	if (m_column)
		delete m_column;
}

void
RemoveColumnCommand::redo()
{
	m_column = m_columnList->removeColumn(m_index);
}

void
RemoveColumnCommand::undo()
{
	Q_ASSERT(m_column != 0);
	m_columnList->insertColumn(m_index, m_column);
	m_column = 0;
}


SwapColumnsCommand::SwapColumnsCommand(ColumnList *columnList, int oldIndex, int newIndex, QUndoCommand *parent)
	: QUndoCommand(parent), m_columnList(columnList), m_oldIndex(oldIndex), m_newIndex(newIndex)
{
}

void
SwapColumnsCommand::redo()
{
	m_columnList->swapColumns(m_oldIndex, m_newIndex);
}

void
SwapColumnsCommand::undo()
{
	m_columnList->swapColumns(m_newIndex, m_oldIndex);
}


