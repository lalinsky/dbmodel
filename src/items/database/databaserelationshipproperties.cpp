// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#include <QDebug>
#include <QComboBox>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include "commands.h"
#include "diagramdocument.h"
#include "column.h"
#include "columnlist.h"
#include "columnlistcombomodel.h"
#include "databaserelationship.h"
#include "databaserelationshipproperties.h"
#include "ui_databaserelationshipproperties.h"

class DatabaseRelationshipProperties::PrivateData
{
public:
	PrivateData()
		: ignoreColumnChanges(true)
		{}
	
	QWidget *nameEdit;
	QCheckBox *generateNameCheckBox;
	QComboBox *cardinalityComboBox;
	Ui_DatabaseRelationshipForm ui;
	QButtonGroup *cardinalityButtonGroup;
	ColumnListComboModel *childColumnModel;
	ColumnListComboModel *parentColumnModel;
	bool ignoreColumnChanges;
};

DatabaseRelationshipProperties::DatabaseRelationshipProperties(QWidget *parent)
	: DiagramItemProperties(parent), d(new PrivateData)
{
	addPage(tr("&Relationship"), createRelationshipPage());
}

QWidget *
DatabaseRelationshipProperties::createRelationshipPage()
{
	QWidget *page = new QWidget(this);
	d->ui.setupUi(page);

	connect(d->ui.automaticNameCheckBox, SIGNAL(toggled(bool)), d->ui.nameEdit, SLOT(setDisabled(bool)));

	d->cardinalityButtonGroup = new QButtonGroup(this);
	d->cardinalityButtonGroup->addButton(d->ui.cardinalityOneToOneRadioButton);
	d->cardinalityButtonGroup->addButton(d->ui.cardinalityOneToManyRadioButton);
	d->cardinalityButtonGroup->addButton(d->ui.cardinalityManyToManyRadioButton);
	connect(d->cardinalityButtonGroup, SIGNAL(buttonClicked(QAbstractButton *)),
		SLOT(setCardinality(QAbstractButton *)));

	connect(d->ui.modalityChildCheckBox, SIGNAL(toggled(bool)), SLOT(setChildOptional(bool)));
	connect(d->ui.modalityParentCheckBox, SIGNAL(toggled(bool)), SLOT(setParentOptional(bool)));

	d->childColumnModel = new ColumnListComboModel(this);
	d->ui.childColumnComboBox->setModel(d->childColumnModel);
	d->parentColumnModel = new ColumnListComboModel(this);
	d->ui.parentColumnComboBox->setModel(d->parentColumnModel);

	connect(d->ui.childColumnComboBox, SIGNAL(currentIndexChanged(int)), SLOT(setChildColumn(int)));
	connect(d->ui.parentColumnComboBox, SIGNAL(currentIndexChanged(int)), SLOT(setParentColumn(int)));

	return page;
}

void
DatabaseRelationshipProperties::switchCurrentItem(DiagramItem *oldItem, DiagramItem *)
{
	if (oldItem)
		disconnect(oldItem, 0, this, 0);
	d->ignoreColumnChanges = true;
	DatabaseRelationship *relationship = currentRelationship();
	if (relationship) {
		updateCardinality();
		updateChildOptional();
		updateParentOptional();
		d->childColumnModel->setColumnList(relationship->childTable()->columnList());
		d->parentColumnModel->setColumnList(relationship->parentTable()->columnList());
		d->ui.childColumnComboBox->updateGeometry();
		d->ui.parentColumnComboBox->updateGeometry();
		updateChildColumn();
		updateParentColumn();
		connect(relationship, SIGNAL(propertyChanged(const QString &, const QVariant &)), SLOT(updateProperty(const QString &, const QVariant &)));
		d->ignoreColumnChanges = false;
	}
	else {
		d->childColumnModel->setColumnList(0);
		d->parentColumnModel->setColumnList(0);
	}
}

void
DatabaseRelationshipProperties::updateProperty(const QString &name, const QVariant &)
{
	if (name == "cardinality") {
		updateCardinality();
	}
	else if (name == "childOptional") {
		updateChildOptional();
	}
	else if (name == "childOptional") {
		updateParentOptional();
	}
}

void
DatabaseRelationshipProperties::updateCardinality()
{
	DatabaseRelationship *relationship = currentRelationship();
	switch (relationship->cardinality()) {
		case DatabaseRelationship::OneToOne:
			d->ui.cardinalityOneToOneRadioButton->setChecked(true);
			break;
		case DatabaseRelationship::OneToMany:
			d->ui.cardinalityOneToManyRadioButton->setChecked(true);
			break;
		case DatabaseRelationship::ManyToMany:
			d->ui.cardinalityManyToManyRadioButton->setChecked(true);
			break;
	}
}

void
DatabaseRelationshipProperties::updateChildOptional()
{
	DatabaseRelationship *relationship = currentRelationship();
	d->ui.modalityChildCheckBox->setChecked(relationship->isChildOptional());
}

void
DatabaseRelationshipProperties::updateParentOptional()
{
	DatabaseRelationship *relationship = currentRelationship();
	d->ui.modalityParentCheckBox->setChecked(relationship->isParentOptional());
}

void
DatabaseRelationshipProperties::updateChildColumn()
{
	DatabaseRelationship *relationship = currentRelationship();
	Column *column = relationship->childColumn();
	int index = column ? 1 + relationship->childTable()->columnList()->indexOf(column) : 0;
	d->ui.childColumnComboBox->setCurrentIndex(index);
}

void
DatabaseRelationshipProperties::updateParentColumn()
{
	DatabaseRelationship *relationship = currentRelationship();
	Column *column = relationship->parentColumn();
	int index = column ? 1 + relationship->parentTable()->columnList()->indexOf(column) : 0;
	d->ui.parentColumnComboBox->setCurrentIndex(index);
}

DatabaseRelationship *
DatabaseRelationshipProperties::currentRelationship()
{
	return static_cast<DatabaseRelationship *>(currentItem());
}

void
DatabaseRelationshipProperties::setCardinality(QAbstractButton *button)
{
	DatabaseRelationship::Cardinality cardinality;
	if (button == d->ui.cardinalityOneToOneRadioButton)
		cardinality = DatabaseRelationship::OneToOne;
	else if (button == d->ui.cardinalityOneToManyRadioButton)
		cardinality = DatabaseRelationship::OneToMany;
	else if (button == d->ui.cardinalityManyToManyRadioButton)
		cardinality = DatabaseRelationship::ManyToMany;

	DatabaseRelationship *relationship = currentRelationship();
	relationship->document()->undoStack()->push(
		new SetObjectPropertyCommand(relationship, "cardinality", cardinality));
}

void
DatabaseRelationshipProperties::setChildOptional(bool optional)
{
	DatabaseRelationship *relationship = currentRelationship();
	relationship->document()->undoStack()->push(
		new SetObjectPropertyCommand(relationship, "childOptional", optional));
}

void
DatabaseRelationshipProperties::setParentOptional(bool optional)
{
	DatabaseRelationship *relationship = currentRelationship();
	relationship->document()->undoStack()->push(
		new SetObjectPropertyCommand(relationship, "parentOptional", optional));
}

void
DatabaseRelationshipProperties::setChildColumn(int index)
{
	if (d->ignoreColumnChanges)
		return;
	DatabaseRelationship *relationship = currentRelationship();
	Column *column = index > 0 ? relationship->childTable()->columnList()->column(index - 1) : NULL;
	relationship->document()->undoStack()->push(
		new SetObjectPropertyCommand(relationship, "childColumn", qVariantFromValue(column)));
}

void
DatabaseRelationshipProperties::setParentColumn(int index)
{
	if (d->ignoreColumnChanges)
		return;
	DatabaseRelationship *relationship = currentRelationship();
	Column *column = index > 0 ? relationship->parentTable()->columnList()->column(index - 1) : NULL;
	relationship->document()->undoStack()->push(
		new SetObjectPropertyCommand(relationship, "parentColumn", qVariantFromValue(column)));
}
