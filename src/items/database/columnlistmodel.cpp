// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#include <QDebug>
#include "columnlistmodel.h"
#include "diagramdocument.h"
#include "databasecommands.h"
#include "databasetable.h"
#include "commands.h"
#include "column.h"
#include "columnlist.h"

#define COLUMN_COUNT 5

ColumnListModel::ColumnListModel(QObject *parent)
	: QAbstractTableModel(parent), m_columnList(0)
{
}

void
ColumnListModel::setColumnList(ColumnList *columnList)
{
	if (m_columnList) {
		disconnect(m_columnList, 0, this, 0);
	}
	m_columnList = columnList;
	if (m_columnList) {
		connect(m_columnList, SIGNAL(columnAboutToBeInserted(int)), this, SLOT(_columnAboutToBeInserted(int)));
		connect(m_columnList, SIGNAL(columnInserted(int)), this, SLOT(_columnInserted()));
		connect(m_columnList, SIGNAL(columnAboutToBeRemoved(int)), this, SLOT(_columnAboutToBeRemoved(int)));
		connect(m_columnList, SIGNAL(columnRemoved(int)), this, SLOT(_columnRemoved()));
		connect(m_columnList, SIGNAL(columnChanged(int)), this, SLOT(_columnChanged(int)));
	}
	reset();
}

int
ColumnListModel::rowCount(const QModelIndex &parent) const
{
	if (parent.isValid())
		return 0;
	return m_columnList ? m_columnList->columnCount() + 1 : 0;
}

int
ColumnListModel::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid())
		return 0;
	return COLUMN_COUNT;
}

QVariant
ColumnListModel::data(const QModelIndex &index, int role) const
{
	if (m_columnList) {
		Q_ASSERT(index.isValid());
		int row = index.row() - firstRow();
		if (row < 0 || row >= m_columnList->columnCount()) {
			return QVariant();
		}
		Column *column = m_columnList->column(row);
		if (role == Qt::DisplayRole || role == Qt::EditRole) {
			if (index.column() == 0) {
				return column->name();
			}
			if (index.column() == 1) {
				return column->dataType();
			}
			if (index.column() == 4) {
				return column->notes();
			}
		}
		if (role == Qt::CheckStateRole) {
			if (index.column() == 2) {
				return column->isRequired() ? Qt::Checked : Qt::Unchecked;
			}
			if (index.column() == 3) {
				return column->isPrimaryKey() ? Qt::Checked : Qt::Unchecked;
			}
		}
	}
	return QVariant();
}

bool
ColumnListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (m_columnList) {
		Q_ASSERT(index.isValid());
		Column *column;
		int row = index.row() - firstRow();
		if (row == m_columnList->columnCount()) {
			column = new Column();
			m_columnList->table()->document()->undoStack()->push(
				new AddColumnCommand(m_columnList, column));
		}
		else {
			column = m_columnList->column(row);
		}
		if (role == Qt::DisplayRole || role == Qt::EditRole) {
			QString text = value.toString();
			if (index.column() == 0) {
				m_columnList->table()->document()->undoStack()->push(
					new SetObjectPropertyCommand(column, "name", text));
				goto OK;
			}
			if (index.column() == 1) {
				m_columnList->table()->document()->undoStack()->push(
					new SetObjectPropertyCommand(column, "dataType", text));
				goto OK;
			}
			if (index.column() == 4) {
				m_columnList->table()->document()->undoStack()->push(
					new SetObjectPropertyCommand(column, "notes", text));
				goto OK;
			}
		}
		if (role == Qt::CheckStateRole) {
			bool checked = value.toInt() == Qt::Checked ? true : false;
			if (index.column() == 2) {
				m_columnList->table()->document()->undoStack()->push(
					new SetObjectPropertyCommand(column, "required", checked));
				goto OK;
			}
			if (index.column() == 3) {
				QUndoCommand *command;
				if (!column->isRequired()) {
					command = new QUndoCommand();
					(void *)new SetObjectPropertyCommand(column, "primaryKey", checked, command);
					(void *)new SetObjectPropertyCommand(column, "required", true, command);
				}
				else {
					command = new SetObjectPropertyCommand(column, "primaryKey", checked);
				}
				m_columnList->table()->document()->undoStack()->push(command);
				goto OK;
			}
		}
	}
	return false;
OK:
	return true;
}

Qt::ItemFlags
ColumnListModel::flags(const QModelIndex &index) const
{
	Q_ASSERT(index.isValid());
	if (index.column() == 0 || index.column() == 1 || index.column() == 4) {
		return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable;
	}
	if (index.column() == 2 || index.column() == 3) {
		return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable;
	}
	return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

QVariant
ColumnListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
		if (section == 0)
			return tr("Name");
		if (section == 1)
			return tr("Data Type");
		if (section == 2)
			return tr("Req'd");
		if (section == 3)
			return tr("PK");
		if (section == 4)
			return tr("Notes");
	}
	return QVariant();
}

QModelIndex
ColumnListModel::indexFromRow(int i) const
{
	return createIndex(firstRow() + i, 0);
}

void
ColumnListModel::_columnAboutToBeInserted(int index)
{
	beginInsertRows(QModelIndex(), firstRow() + index, firstRow() + index);
}

void
ColumnListModel::_columnInserted()
{
	endInsertRows();
}

void
ColumnListModel::_columnAboutToBeRemoved(int index)
{
	beginRemoveRows(QModelIndex(), firstRow() + index, firstRow() + index);
}

void
ColumnListModel::_columnRemoved()
{
	endRemoveRows();
}

void
ColumnListModel::_columnChanged(int index)
{
	emit dataChanged(createIndex(firstRow() + index, 0), createIndex(firstRow() + index, COLUMN_COUNT));
}
