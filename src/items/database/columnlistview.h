// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#ifndef COLUMNLISTVIEW_H
#define COLUMNLISTVIEW_H

#include <QTreeView>
#include "columnlistmodel.h"

class ColumnListView : public QTreeView
{
	Q_OBJECT

public:
	ColumnListView(QWidget *parent = 0);

	void setColumnList(ColumnList *columnList);
	ColumnListModel *columnListModel() { return qobject_cast<ColumnListModel *>(model()); }

	ColumnList *columnList() { return columnListModel()->columnList(); }

	QList<int> selectedColumns() const;

public slots:
	void addColumn();
	void removeColumn();
	void moveColumnUp();
	void moveColumnDown();

protected:
	QModelIndexList selectedIndexes() const;
};

#endif
