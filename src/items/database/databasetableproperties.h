// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#ifndef DATABASETABLEPROPERTIES_H
#define DATABASETABLEPROPERTIES_H

#include "diagramitemproperties.h"
class DatabaseTable;

class DatabaseTableProperties : public DiagramItemProperties
{
	Q_OBJECT

public:
	DatabaseTableProperties(QWidget *parent = 0);
	DatabaseTable *currentTable();

protected:
	void switchCurrentItem(DiagramItem *oldItem, DiagramItem *newItem);

protected slots:
	void updateProperty(const QString &name, const QVariant &value);
	void updateColumnSelection();
	void setTableName(const QString &name);
	void setTableColor(const QColor &color);

private:
	class PrivateData;
	PrivateData *d;

	QWidget *createTablePage();
	QWidget *createColumnsPage();
};

#endif
