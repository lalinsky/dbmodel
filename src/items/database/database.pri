DEPENDPATH += $$PWD

SOURCES += \
	databasecommands.cpp \
	column.cpp \
	columnlist.cpp \
	columnlistcombomodel.cpp \
	columnlistmodel.cpp \
	columnlistview.cpp \
	databasetable.cpp \
	databasetableproperties.cpp \
	databaserelationship.cpp \
	databaserelationshipproperties.cpp

HEADERS += \
	databasecommands.h \
	column.h \
	columnlist.h \
	columnlistcombomodel.h \
	columnlistmodel.h \
	columnlistview.h \
	databasetable.h \
	databasetableproperties.h \
	databaserelationship.h \
	databaserelationshipproperties.h

FORMS += \
	databaserelationshipproperties.ui
