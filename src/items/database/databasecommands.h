// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#ifndef DATABASECOMMANDS_H
#define DATABASECOMMANDS_H

#include "../../commands.h"

class Column;
class ColumnList;
class DatabaseTable;

class AddColumnCommand : public QUndoCommand
{
public:
	AddColumnCommand(ColumnList *columnList, Column *column = 0, QUndoCommand *parent = 0);
	~AddColumnCommand();
	void undo();
	void redo();
	int index() const { return m_index; }

private:
	ColumnList *m_columnList;
	int m_index;
	Column *m_column;
};

class RemoveColumnCommand : public QUndoCommand
{
public:
	RemoveColumnCommand(ColumnList *columnList, int index, QUndoCommand *parent = 0);
	~RemoveColumnCommand();
	void undo();
	void redo();

private:
	ColumnList *m_columnList;
	int m_index;
	Column *m_column;
};

class SwapColumnsCommand : public QUndoCommand
{
public:
	SwapColumnsCommand(ColumnList *columnList, int oldIndex, int newIndex, QUndoCommand *parent = 0);
	void undo();
	void redo();

private:
	ColumnList *m_columnList;
	int m_oldIndex, m_newIndex;
};

#endif
