// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#ifndef DATABASERELATIONSHIPPROPERTIES_H
#define DATABASERELATIONSHIPPROPERTIES_H

#include "diagramitemproperties.h"
class DatabaseRelationship;
class QAbstractButton;

class DatabaseRelationshipProperties : public DiagramItemProperties
{
	Q_OBJECT

public:
	DatabaseRelationshipProperties(QWidget *parent = 0);
	DatabaseRelationship *currentRelationship();

protected:
	void switchCurrentItem(DiagramItem *oldItem, DiagramItem *newItem);
	void updateCardinality();
	void updateChildOptional();
	void updateParentOptional();
	void updateChildColumn();
	void updateParentColumn();

protected slots:
	void updateProperty(const QString &name, const QVariant &value);
	void setCardinality(QAbstractButton *button);
	void setChildOptional(bool);
	void setParentOptional(bool);
	void setChildColumn(int);
	void setParentColumn(int);

private:
	class PrivateData;
	PrivateData *d;

	QWidget *createRelationshipPage();
};

#endif
