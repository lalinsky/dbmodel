// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#ifndef DOMUTILS_H
#define DOMUTILS_H

#include <QDebug>
#include <QDomDocument>
#include <QDomElement>
#include <QMetaEnum>
#include <QMetaObject>

inline void
appendStringElement(QDomDocument &doc, QDomElement &parent, const QString &name, const QString &value)
{
	if (!value.isEmpty()) {
		QDomElement element = doc.createElement(name);
		element.appendChild(doc.createTextNode(value));
		parent.appendChild(element);
	}
}

inline QString
readStringElement(QDomElement &parent, const QString &name, const QString &defaultValue = QString())
{
	QDomElement element = parent.firstChildElement(name);
	if (!element.isNull()) {
		return element.text();
	}
	return defaultValue;
}

inline const char *
enumNameFromValue(const QObject *obj, const char *enumerator, int value)
{
	const QMetaObject *metaObj = obj->metaObject();
	int enumeratorIndex = metaObj->indexOfEnumerator(enumerator);
	return metaObj->enumerator(enumeratorIndex).valueToKey(value);
}

inline int
enumValueFromName(const QObject *obj, const char *enumerator, const char *key)
{
	const QMetaObject *metaObj = obj->metaObject();
	int enumeratorIndex = metaObj->indexOfEnumerator(enumerator);
	return metaObj->enumerator(enumeratorIndex).keyToValue(key);
}

template <typename T> inline void
appendEnumElement(QDomDocument doc, QDomElement element, const QString &name, T value, const QObject *obj, const char *enumName)
{
	appendStringElement(doc, element, name, enumNameFromValue(obj, enumName, value));
}

template <typename T> inline T
readEnumElement(QDomElement &parent, const QString &name, T defaultValue, const QObject *obj, const char *enumName)
{
	QDomElement element = parent.firstChildElement(name);
	if (!element.isNull()) {
		return T(enumValueFromName(obj, enumName, element.text().toLatin1().constData()));
	}
	return defaultValue;
}

inline void
appendBoolElement(QDomDocument doc, QDomElement element, const QString &name, bool value, const QString &trueName = "True", const QString &falseName = "False")
{
	appendStringElement(doc, element, name, value ? trueName : falseName);
}

inline bool
readBoolElement(QDomElement &parent, const QString &name, bool defaultValue = false, const QString &trueName = "True", const QString &falseName = "False")
{
	QDomElement element = parent.firstChildElement(name);
	if (!element.isNull()) {
		QString value = element.text();
		if (value == trueName) {
			return true;
		}
		else if (value == falseName) {
			return false;
		}
		else {
			qWarning() << "Invalid value for" << name <<  ":" << value << "(must be either" << trueName << "or" << falseName << ")";
		}
	}
	return defaultValue;
}

inline void
appendFloatElement(QDomDocument &doc, QDomElement &parent, const QString &name, qreal value)
{
	QDomElement element = doc.createElement(name);
	element.appendChild(doc.createTextNode(QString::number(double(value))));
	parent.appendChild(element);
}

inline qreal
readFloatElement(QDomElement &parent, const QString &name, qreal defaultValue = 0)
{
	QDomElement element = parent.firstChildElement(name);
	if (!element.isNull()) {
		return element.text().toDouble();
	}
	return defaultValue;
}

inline void
appendIntElement(QDomDocument &doc, QDomElement &parent, const QString &name, int value)
{
	QDomElement element = doc.createElement(name);
	element.appendChild(doc.createTextNode(QString::number(value)));
	parent.appendChild(element);
}

inline int
readIntElement(QDomElement &parent, const QString &name, int defaultValue = 0)
{
	QDomElement element = parent.firstChildElement(name);
	if (!element.isNull()) {
		return element.text().toInt();
	}
	return defaultValue;
}

inline void
appendPointElement(QDomDocument &doc, QDomElement &parent, const QString &name, QPointF value)
{
	if (!value.isNull()) {
		QDomElement element = doc.createElement(name);
		appendFloatElement(doc, element, "x", value.x());
		appendFloatElement(doc, element, "y", value.y());
		parent.appendChild(element);
	}
}

inline QPointF
readPointElement(QDomElement &parent, const QString &name)
{
	QDomElement element = parent.firstChildElement(name);
	if (!element.isNull()) {
		qreal x = readFloatElement(element, "x");
		qreal y = readFloatElement(element, "y");
		return QPointF(x, y);
	}
	return QPointF();
}

#endif
