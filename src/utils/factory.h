// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#ifndef FACTORY_H
#define FACTORY_H

#include <QMap>
#include <QStringList>
#include "singelton.h"

template <typename TI>
class FactoryHelperBase
{
public:
	virtual TI *create() = 0;
};

template <typename TI, typename ST>
class FactoryHelper : public FactoryHelperBase<TI>
{
public:
	TI *create()
	{
		return new ST();
	}
};

template <typename T, typename TI, typename Helper>
class Factory : public Singelton<T>
{
public:
	static TI *create(const QString &key)
	{
		T *inst = T::instance();
		if (!inst->m_map.contains(key))
			return NULL;
		return inst->m_map[key].create();
	}
	static QStringList keys()
	{
		T *inst = T::instance();
		return inst->m_map.keys();
	}
protected:
	template <typename ST> void add() { m_map[ST::staticTypeName()] = Helper<TI, ST>(); }
	typedef TI *(*CreateFunc)();
	QMap<QString, FactoryHelperBase> m_map;
};

#define FACTORY_ADD_TYPE(ST) add<ST>();

#endif
