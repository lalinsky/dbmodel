// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#include <QDebug>

template <class T>
class Range {
public:
	Range() : m_min(0), m_max(0) {}
	Range(T min, T max)
	{
		Q_ASSERT(min <= max);
		m_min = min;
		m_max = max;
	}

	T min() const { return m_min; }
	T max() const { return m_max; }

	bool isNull() const { return m_max == m_min; }

	T length() const { return m_max - m_min; }
	T center() const { return m_min + length() / 2; }

	Range<T> intersected(const Range<T> &other) const
	{
		const Range<T> *a = this;
		const Range<T> *b = &other;

		if (a->isNull()) return *a;
		if (b->isNull()) return *b;
		
		if (a->min() > b->min())
			qSwap(a, b);
	
		if (a->max() < b->min())
			return Range<T>();

		T p1 = b->min();
		T p2 = qMin(a->max(), b->max());

		return Range<T>(p1, p2);
	}

private:
	T m_min;
	T m_max;
};


template <class T> inline
QDebug operator<<(QDebug dbg, const Range<T> &r)
{
	dbg.nospace() << "Range(" << r.min() << ", " << r.max() << ")";
	return dbg.space();
}
