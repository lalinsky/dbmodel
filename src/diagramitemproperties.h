// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#ifndef DIAGRAMITEMPROPERTIES_H
#define DIAGRAMITEMPROPERTIES_H

#include <QTabWidget>
class DiagramItem;

class DiagramItemProperties : public QTabWidget
{
	Q_OBJECT

public:
	DiagramItemProperties(QWidget *parent = 0);
	~DiagramItemProperties();

	DiagramItem *currentItem() const;

public slots:
	void setCurrentItem(DiagramItem *item);

protected:
	int addPage(const QString &label, QWidget *page);
	virtual void switchCurrentItem(DiagramItem *oldItem, DiagramItem *newItem);

private:
	class DiagramItemPropertiesPrivate;
	DiagramItemPropertiesPrivate *d;
};

#endif
