// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#include <QDebug>
#include <QLocale>
#include <QTranslator>
#include <QApplication>
#include "mainwindow.h"

class Column;

int
main(int argc, char **argv)
{
    Q_INIT_RESOURCE(dbmodel);
	QApplication app(argc, argv);

	qRegisterMetaType<Column*>("Column*");

	QCoreApplication::setOrganizationName("Lukas Lalinsky");
	QCoreApplication::setApplicationName("Database Modeller");

	QTranslator translator;
	translator.load("dbmodel_" + QLocale::system().name(), ":/translations/");
	app.installTranslator(&translator);

	QString fileName;
	QStringList args = app.arguments();
	if (args.size() > 1) {
		fileName = args.at(1);
	}
	
	MainWindow window(fileName);
	window.show();
	return app.exec();
}
