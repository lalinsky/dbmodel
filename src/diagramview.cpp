// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#include <cmath>
#include <QMouseEvent>
#include <QScrollBar>
#include <QTimer>
#include <QDebug>
#include "diagramview.h"
#include "diagramdocument.h"

using namespace std;

DiagramView::DiagramView(QWidget *parent)
    : QGraphicsView(parent),
        m_handScrolling(false),
        m_autoScrollCount(0),
        m_autoScrollMargin(16)
{
	setAlignment(Qt::AlignLeft | Qt::AlignTop);
	setRenderHint(QPainter::Antialiasing);
	setDragMode(QGraphicsView::RubberBandDrag);
	setRubberBandSelectionMode(Qt::ContainsItemBoundingRect);
	m_autoScrollTimer = new QTimer(this);
	connect(m_autoScrollTimer, SIGNAL(timeout()), SLOT(doAutoScroll()));
}

DiagramDocument *
DiagramView::document() const
{
	return static_cast<DiagramDocument *>(scene());
}

void
DiagramView::setScene(QGraphicsScene *scene)
{
	QGraphicsView::setScene(scene);
	updateSceneRect2(scene->sceneRect());
	connect(scene, SIGNAL(sceneRectChanged(const QRectF &)), SLOT(updateSceneRect2(const QRectF &)));
}

void
DiagramView::updateSceneRect2(const QRectF &rect)
{
	QRectF sceneRect = rect.united(QRectF(QPointF(0, 0), QPointF(100, 100)));
	setSceneRect(sceneRect);
}

void
DiagramView::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::MidButton) {
		m_handScrolling = true;
        m_handScrollingOrigin = QPoint(event->pos());
		m_handScrollingValueX = horizontalScrollBar()->value();
        m_handScrollingValueY = verticalScrollBar()->value();
		m_savedCursor = viewport()->cursor();
		viewport()->setCursor(Qt::ClosedHandCursor);
		event->accept();
		return;
	}
	QGraphicsView::mousePressEvent(event);
}

void
DiagramView::mouseMoveEvent(QMouseEvent *event)
{
	if (m_handScrolling) {
		QPoint delta = event->pos() - m_handScrollingOrigin;
		horizontalScrollBar()->setValue(m_handScrollingValueX - delta.x());
		verticalScrollBar()->setValue(m_handScrollingValueY - delta.y());
		event->accept();
		return;
	}
	QGraphicsView::mouseMoveEvent(event);

    if (event->buttons() && !isAutoScrolling() && shouldAutoScroll(event->pos())) {
        startAutoScroll();
    }
}

void
DiagramView::mouseReleaseEvent(QMouseEvent *event)
{
	if (m_handScrolling) {
		m_handScrolling = false;
		viewport()->setCursor(m_savedCursor);
		event->accept();
		return;
	}
	QGraphicsView::mouseReleaseEvent(event);
}

void
DiagramView::drawBackground(QPainter *painter, const QRectF &rect)
{
	DiagramDocument *doc = document();
	if (doc && doc->isGridVisible()) {
		drawGrid(painter, rect);
	}
}

void
DiagramView::drawGrid(QPainter *painter, const QRectF &rect)
{
	const int pointBufferSize = 5000;
	static QPoint pointBuffer[pointBufferSize];
	DiagramDocument *doc = document();
	int gridSize = doc->gridSize();
	int x0 = gridSize * floor(rect.left() / gridSize);
	int y0 = gridSize * floor(rect.top() / gridSize);
	int x1 = gridSize * ceil(rect.right() / gridSize);
	int y1 = gridSize * ceil(rect.bottom() / gridSize);
	painter->save();
	QPen pen;
	pen.setColor(doc->gridColor());
	pen.setWidth(0);
	painter->setPen(pen);
	painter->setRenderHint(QPainter::Antialiasing, false);
	int pointsUsed = 0;
	for (int x = x0; x < x1; x += gridSize) {
		for (int y = y0; y < y1; y += gridSize) {
			pointBuffer[pointsUsed].setX(x);
			pointBuffer[pointsUsed].setY(y);
			pointsUsed++;
			if (pointsUsed == pointBufferSize) {
				painter->drawPoints(pointBuffer, pointsUsed);
				pointsUsed = 0;
			}
		}
	}
	if (pointsUsed > 0) {
		painter->drawPoints(pointBuffer, pointsUsed);
		pointsUsed = 0;
	}
	painter->restore();
}

static QRect
clipRect(QWidget *w)
{
    if (!w->isVisible())
        return QRect();
    QRect r = w->rect();
    int ox = 0;
    int oy = 0;
    while (w
            && w->isVisible()
            && !w->isWindow()
            && w->parentWidget()) {
        ox -= w->x();
        oy -= w->y();
        w = w->parentWidget();
        r &= QRect(ox, oy, w->width(), w->height());
    }
    return r;
}

void
DiagramView::startAutoScroll()
{
    m_autoScrollTimer->start(50);
    m_autoScrollCount = 0;
}

void
DiagramView::stopAutoScroll()
{
    m_autoScrollTimer->stop();
    m_autoScrollCount = 0;
}

bool
DiagramView::isAutoScrolling() const
{
    return m_autoScrollTimer->isActive();
}

bool
DiagramView::shouldAutoScroll(const QPoint &pos) const
{
    QRect area = clipRect(viewport());
    return (pos.y() - area.top() < m_autoScrollMargin)
        || (area.bottom() - pos.y() < m_autoScrollMargin)
        || (pos.x() - area.left() < m_autoScrollMargin)
        || (area.right() - pos.x() < m_autoScrollMargin);
}

void
DiagramView::doAutoScroll()
{
    int verticalStep = verticalScrollBar()->pageStep();
    int horizontalStep = horizontalScrollBar()->pageStep();
    if (m_autoScrollCount < qMax(verticalStep, horizontalStep))
        ++m_autoScrollCount;

    int verticalValue = verticalScrollBar()->value();
    int horizontalValue = horizontalScrollBar()->value();

    QPoint pos = viewport()->mapFromGlobal(QCursor::pos());
    QRect area = clipRect(viewport());

    if (pos.y() - area.top() < m_autoScrollMargin)
        verticalScrollBar()->setValue(verticalValue - m_autoScrollCount);
    else if (area.bottom() - pos.y() < m_autoScrollMargin)
        verticalScrollBar()->setValue(verticalValue + m_autoScrollCount);
    if (pos.x() - area.left() < m_autoScrollMargin)
        horizontalScrollBar()->setValue(horizontalValue - m_autoScrollCount);
    else if (area.right() - pos.x() < m_autoScrollMargin)
        horizontalScrollBar()->setValue(horizontalValue + m_autoScrollCount);

    bool verticalUnchanged = (verticalValue == verticalScrollBar()->value());
    bool horizontalUnchanged = (horizontalValue == horizontalScrollBar()->value());
    if (verticalUnchanged && horizontalUnchanged) {
        stopAutoScroll();
    } else {
        viewport()->update();
    }
}
