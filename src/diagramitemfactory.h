// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#ifndef DIAGRAMITEMFACTORY_H
#define DIAGRAMITEMFACTORY_H

#include <QMap>
#include <QStringList>
#include "utils/singelton.h"
class DiagramItem;
class DiagramItemProperties;

class DiagramItemFactory : public Singelton<DiagramItemFactory>
{

	class CreatorBase
	{
	public:
		virtual DiagramItem *create() = 0;
		virtual DiagramItemProperties *createPropertiesEditor(QWidget *parent = 0) = 0;
	};

	template <typename ItemType>
	class Creator : public CreatorBase
	{
	public:
		DiagramItem *create()
		{ return new ItemType(); }
		DiagramItemProperties *createPropertiesEditor(QWidget *parent = 0)
		{ return ItemType::createPropertiesEditor(parent); }
	};

public:

	static DiagramItem *create(const QString &key)
	{
		DiagramItemFactory *inst = DiagramItemFactory::instance();
		if (!inst->m_map.contains(key))
			return NULL;
		return inst->m_map[key]->create();
	}

	static DiagramItemProperties *createPropertiesEditor(const QString &key, QWidget *parent = 0)
	{
		DiagramItemFactory *inst = DiagramItemFactory::instance();
		if (!inst->m_map.contains(key))
			return NULL;
		return inst->m_map[key]->createPropertiesEditor(parent);
	}

	static QStringList keys()
	{
		DiagramItemFactory *inst = DiagramItemFactory::instance();
		return inst->m_map.keys();
	}

protected:

	template <typename Type> void add()
	{
		m_map[Type::staticTypeName()] = new Creator<Type>();
	}

	QMap<QString, CreatorBase*> m_map;

	DiagramItemFactory();
	friend class Singelton<DiagramItemFactory>;
};

#define FACTORY_ADD_TYPE(ST) add<ST>();

#endif
