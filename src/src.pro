TARGET = dbmodel
VERSION = 0.3

DESTDIR = ../

QT += xml svg
#CONFIG += debug

DEFINES += VERSION=\\\"$$VERSION\\\"

include(src.pri)
include(diagram/diagram.pri)
include(items/items.pri)
include(utils/utils.pri)
include(export/export.pri)

include(../updateqm.pri)

MOC_DIR = $$PWD/.build
OBJECTS_DIR = $$PWD/.build
UI_DIR = $$PWD/.build
RCC_DIR = $$PWD/.build

unix {
    isEmpty(PREFIX) {
        PREFIX = /usr/local
    }

    BINDIR = $$PREFIX/bin
    DATADIR = $$PREFIX/share

    INSTALLS += target man man-compress

    target.path = $$BINDIR

    man.path = $$DATADIR/man/man1
    man.files += ../data/dbmodel.1

    man-compress.path = $$DATADIR/man/man1
    man-compress.extra = "" "gzip -9 -f \$(INSTALL_ROOT)/$$DATADIR/man/man1/dbmodel.1" ""
    man-compress.depends = install_man
}

# It's required for windows correct include path.
# Dunno why...
win32 {
    INCLUDEPATH += .
}
