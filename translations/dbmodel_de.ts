<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de">
<context>
    <name>ColorPickerPopup</name>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="897"/>
        <source>Custom</source>
        <translation>Benutzerdefiniert</translation>
    </message>
</context>
<context>
    <name>ColumnListModel</name>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="177"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="179"/>
        <source>Data Type</source>
        <translation>Datentyp</translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="181"/>
        <source>Req&apos;d</source>
        <translation>Benötigt</translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="183"/>
        <source>PK</source>
        <translation>Primärschlüssel</translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="185"/>
        <source>Notes</source>
        <translation>Notizen</translation>
    </message>
</context>
<context>
    <name>DatabaseRelationshipForm</name>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="16"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="28"/>
        <source>Automatic</source>
        <translation>automatisch</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="37"/>
        <source>Cardinality:</source>
        <translation>Kardinalität:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="46"/>
        <source>1:1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="53"/>
        <source>1:N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="60"/>
        <source>M:N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="82"/>
        <source>Modality:</source>
        <translation>Modalität:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="91"/>
        <source>Parent is optional</source>
        <translation>Eltern sind optional</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="98"/>
        <source>Child is optional</source>
        <translation>Kind ist optional</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="120"/>
        <source>Columns:</source>
        <translation>Spalten:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="139"/>
        <source>references</source>
        <translation>Referenz:</translation>
    </message>
</context>
<context>
    <name>DatabaseRelationshipProperties</name>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.cpp" line="53"/>
        <source>&amp;Relationship</source>
        <translation>&amp;Beziehung</translation>
    </message>
</context>
<context>
    <name>DatabaseTableProperties</name>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="50"/>
        <source>&amp;Table</source>
        <translation>&amp;Tabelle</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="51"/>
        <source>&amp;Columns</source>
        <translation>&amp;Spalten</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="68"/>
        <source>Name:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="72"/>
        <source>White</source>
        <translation>weiß</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="73"/>
        <location filename="../src/items/database/databasetableproperties.cpp" line="74"/>
        <location filename="../src/items/database/databasetableproperties.cpp" line="75"/>
        <source>Gray</source>
        <translation>grau</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="76"/>
        <source>Red</source>
        <translation>rot</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="77"/>
        <source>Brown</source>
        <translation>braun</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="78"/>
        <source>Pink</source>
        <translation>pink</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="79"/>
        <source>Yellow</source>
        <translation>gelb</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="80"/>
        <source>Green</source>
        <translation>grün</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="81"/>
        <source>Blue</source>
        <translation>blau</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="83"/>
        <source>Color:</source>
        <translation>Farbe:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="106"/>
        <source>&amp;Add</source>
        <translation>&amp;Hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="107"/>
        <source>&amp;Remove</source>
        <translation>&amp;Entfernen</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="108"/>
        <source>Move &amp;Up</source>
        <translation>&amp;hochschieben</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="109"/>
        <source>Move &amp;Down</source>
        <translation>&amp;runterschieben</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="166"/>
        <source>&amp;Properties</source>
        <translation>&amp;Eigenschaften</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="204"/>
        <source>&amp;New</source>
        <translation>&amp;Neu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="211"/>
        <source>&amp;Open...</source>
        <translation>&amp;Öffnen...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="217"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="224"/>
        <source>Save &amp;As...</source>
        <translation>Speichern &amp;unter...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="234"/>
        <source>E&amp;xport...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="238"/>
        <source>Page Set&amp;up...</source>
        <translation>Seite &amp;einrichten...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="242"/>
        <source>&amp;Print...</source>
        <translation>&amp;Drucken...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="248"/>
        <source>Print Previe&amp;w...</source>
        <translation>Druck&amp;vorschau...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="260"/>
        <source>Select</source>
        <translation>Auswählen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="266"/>
        <source>Add new table</source>
        <translation>Tabelle hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="271"/>
        <source>Add new relation</source>
        <translation>Relation hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="275"/>
        <source>&amp;Undo</source>
        <translation>&amp;Rückgängig</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="278"/>
        <source>Re&amp;do</source>
        <translation>&amp;Wiederholen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="287"/>
        <source>Cu&amp;t</source>
        <translation>&amp;Ausschneiden</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="293"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopieren</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="299"/>
        <source>&amp;Paste</source>
        <translation>&amp;Einfügen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="305"/>
        <source>&amp;Delete</source>
        <translation>&amp;Löschen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="311"/>
        <source>&amp;About...</source>
        <translation>&amp;Über...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="315"/>
        <source>&amp;Close</source>
        <translation>&amp;Schließen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="321"/>
        <source>&amp;Quit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="327"/>
        <source>Show &amp;Grid</source>
        <translation>Zeige &amp;Raster</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="331"/>
        <source>&amp;Notation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="334"/>
        <source>&amp;Relational</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="338"/>
        <source>&amp;Crow&apos;s Foot</source>
        <translation>&amp;Krähenfuß-Notation</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="355"/>
        <location filename="../src/mainwindow.cpp" line="404"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="370"/>
        <source>&amp;Mode</source>
        <translation>&amp;Modus</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>50%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>70%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>85%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>100%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>125%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>150%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="386"/>
        <location filename="../src/mainwindow.cpp" line="434"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="424"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="439"/>
        <source>&amp;Diagram</source>
        <translation>&amp;Diagramm</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="443"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="462"/>
        <location filename="../src/mainwindow.cpp" line="529"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="462"/>
        <location filename="../src/mainwindow.cpp" line="529"/>
        <source>Unknown format.</source>
        <translation>Unbekanntes Format.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="481"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation>Das Dokument wurde geändert.
Wollen Sie die Änderungen speichern?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="769"/>
        <source>&amp;%1. %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="826"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="826"/>
        <source>&lt;p&gt;
&lt;b&gt;Database Modeller </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="841"/>
        <source>Untitled</source>
        <translation>Unbenannt</translation>
    </message>
</context>
<context>
    <name>QtColorPicker</name>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="279"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="405"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="508"/>
        <source>Black</source>
        <translation>schwarz</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="406"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="509"/>
        <source>White</source>
        <translation>weiß</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="407"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="510"/>
        <source>Red</source>
        <translation>rot</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="408"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="511"/>
        <source>Dark red</source>
        <translation>dunkelrot</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="409"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="512"/>
        <source>Green</source>
        <translation>grün</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="410"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="513"/>
        <source>Dark green</source>
        <translation>dunkelgrün</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="411"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="514"/>
        <source>Blue</source>
        <translation>blau</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="412"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="515"/>
        <source>Dark blue</source>
        <translation>dunkelblau</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="413"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="516"/>
        <source>Cyan</source>
        <translation>blaugrün</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="414"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="517"/>
        <source>Dark cyan</source>
        <translation>dunkeles blaugrün</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="415"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="518"/>
        <source>Magenta</source>
        <translation>magentarot</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="416"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="519"/>
        <source>Dark magenta</source>
        <translation>dunkles magentarot</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="417"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="520"/>
        <source>Yellow</source>
        <translation>geld</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="418"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="521"/>
        <source>Dark yellow</source>
        <translation>dunkles gelb</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="419"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="522"/>
        <source>Gray</source>
        <translation>grau</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="420"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="523"/>
        <source>Dark gray</source>
        <translation>dunkelgrau</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="421"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="524"/>
        <source>Light gray</source>
        <translation>hellgrau</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="439"/>
        <source>Custom</source>
        <translation>benutzerdefiniert</translation>
    </message>
</context>
</TS>
