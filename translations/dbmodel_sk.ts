<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="sk">
<context>
    <name>ColorPickerPopup</name>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="897"/>
        <source>Custom</source>
        <translation>Vlastná</translation>
    </message>
</context>
<context>
    <name>ColumnListModel</name>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="177"/>
        <source>Name</source>
        <translation>Názov</translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="179"/>
        <source>Data Type</source>
        <translation>Dátový typ</translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="181"/>
        <source>Req&apos;d</source>
        <translation>Požadovaný</translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="183"/>
        <source>PK</source>
        <translation>Primárny kľúč</translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="185"/>
        <source>Notes</source>
        <translation>Poznámky</translation>
    </message>
</context>
<context>
    <name>DatabaseRelationshipForm</name>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="16"/>
        <source>Name:</source>
        <translation>Názov:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="28"/>
        <source>Automatic</source>
        <translation>Automatické</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="37"/>
        <source>Cardinality:</source>
        <translation>Kardinalita:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="46"/>
        <source>1:1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="53"/>
        <source>1:N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="60"/>
        <source>M:N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="82"/>
        <source>Modality:</source>
        <translation>Modalita:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="91"/>
        <source>Parent is optional</source>
        <translation>Rodič je nepovinný</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="98"/>
        <source>Child is optional</source>
        <translation>Potomok je nepovinný</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="120"/>
        <source>Columns:</source>
        <translation>Stĺpce:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="139"/>
        <source>references</source>
        <translation>odkazuje na</translation>
    </message>
</context>
<context>
    <name>DatabaseRelationshipProperties</name>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.cpp" line="53"/>
        <source>&amp;Relationship</source>
        <translation>&amp;Vzťah</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="obsolete">Názov:</translation>
    </message>
    <message>
        <source>Cardinality:</source>
        <translation type="obsolete">Kardinalita:</translation>
    </message>
</context>
<context>
    <name>DatabaseTableProperties</name>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="50"/>
        <source>&amp;Table</source>
        <translation>&amp;Tabuľka</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="51"/>
        <source>&amp;Columns</source>
        <translation>Stĺ&amp;ce</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="68"/>
        <source>Name:</source>
        <translation>Názov:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="72"/>
        <source>White</source>
        <translation>Biela</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="73"/>
        <location filename="../src/items/database/databasetableproperties.cpp" line="74"/>
        <location filename="../src/items/database/databasetableproperties.cpp" line="75"/>
        <source>Gray</source>
        <translation>Šedá</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="76"/>
        <source>Red</source>
        <translation>Červená</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="77"/>
        <source>Brown</source>
        <translation>Hnedá</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="78"/>
        <source>Pink</source>
        <translation>Ružová</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="79"/>
        <source>Yellow</source>
        <translation>Žtlá</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="80"/>
        <source>Green</source>
        <translation>Zelená</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="81"/>
        <source>Blue</source>
        <translation>Modrá</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="83"/>
        <source>Color:</source>
        <translation>Farba:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="108"/>
        <source>Move &amp;Up</source>
        <translation>Posunúť &amp;hore</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="109"/>
        <source>Move &amp;Down</source>
        <translation>Posunúť &amp;dole</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="106"/>
        <source>&amp;Add</source>
        <translation>Prid&amp;ať</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="107"/>
        <source>&amp;Remove</source>
        <translation>Odst&amp;rániť</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="204"/>
        <source>&amp;New</source>
        <translation>&amp;Nový</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="211"/>
        <source>&amp;Open...</source>
        <translation>&amp;Otvoriť...
</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="217"/>
        <source>&amp;Save</source>
        <translation>&amp;Uložiť</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="224"/>
        <source>Save &amp;As...</source>
        <translation>Uložiť &amp;ako...</translation>
    </message>
    <message>
        <source>Export...</source>
        <translation type="obsolete">Exportovať...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="260"/>
        <source>Select</source>
        <translation>Výber</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="266"/>
        <source>Add new table</source>
        <translation>Pridať novú tabuľku</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="271"/>
        <source>Add new relation</source>
        <translation>Pridať nový vzťah</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="275"/>
        <source>&amp;Undo</source>
        <translation>Vrátit &amp;späť</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="278"/>
        <source>Re&amp;do</source>
        <translation>&amp;Opakovať vrátené</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="355"/>
        <location filename="../src/mainwindow.cpp" line="404"/>
        <source>&amp;File</source>
        <translation>&amp;Súbor</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="370"/>
        <source>&amp;Mode</source>
        <translation>&amp;Mód</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>50%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>70%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>85%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>100%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>125%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>150%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="321"/>
        <source>&amp;Quit</source>
        <translation>&amp;Koniec</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="305"/>
        <source>&amp;Delete</source>
        <translation>&amp;Odstrániť</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="462"/>
        <location filename="../src/mainwindow.cpp" line="529"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="462"/>
        <location filename="../src/mainwindow.cpp" line="529"/>
        <source>Unknown format.</source>
        <translation>Neznámy formát.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="424"/>
        <source>&amp;Edit</source>
        <translation>&amp;Upraviť</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="234"/>
        <source>E&amp;xport...</source>
        <translation>E&amp;xportovať...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="238"/>
        <source>Page Set&amp;up...</source>
        <translation type="unfinished">Nasta&amp;venie stránky...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="242"/>
        <source>&amp;Print...</source>
        <translation>&amp;Tlačiť...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="248"/>
        <source>Print Previe&amp;w...</source>
        <translation>Ukážka pr&amp;ed tlačou...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="287"/>
        <source>Cu&amp;t</source>
        <translation>Vystri&amp;hnúť</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="293"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopírovať</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="299"/>
        <source>&amp;Paste</source>
        <translation>&amp;Vložiť</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="386"/>
        <location filename="../src/mainwindow.cpp" line="434"/>
        <source>&amp;View</source>
        <translation>&amp;Zobraziť</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="481"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation>Dokument bol zmenený. Chcete uložiť Vaše zmeny?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="769"/>
        <source>&amp;%1. %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="311"/>
        <source>&amp;About...</source>
        <translation>&amp;O aplikácii...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="443"/>
        <source>&amp;Help</source>
        <translation>&amp;Pomocník</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="826"/>
        <source>About</source>
        <translation>O aplikácii</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="166"/>
        <source>&amp;Properties</source>
        <translation>&amp;Vlastnosti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="315"/>
        <source>&amp;Close</source>
        <translation>Za&amp;vrieť</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="327"/>
        <source>Show &amp;Grid</source>
        <translation>Zobraziť m&amp;riežku</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="826"/>
        <source>&lt;p&gt;
&lt;b&gt;Database Modeller </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="841"/>
        <source>Untitled</source>
        <translation>Nepomenované</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="331"/>
        <source>&amp;Notation</source>
        <translation>&amp;Notácia</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="334"/>
        <source>&amp;Relational</source>
        <translation>&amp;Relačná</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="338"/>
        <source>&amp;Crow&apos;s Foot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="439"/>
        <source>&amp;Diagram</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;p&gt;
&lt;b&gt;Database Modeller&lt;/b&gt;&lt;br /&gt;
&lt;a href=&quot;http://oxygene.sk/lukas/dbmodel/&quot;&gt;http://oxygene.sk/lukas/dbmodel/&lt;/a&gt;&lt;br /&gt;
Copyright (C) 2008 Lukas Lalinsky
&lt;/p&gt;
</source>
        <translation type="obsolete">&lt;p&gt;&lt;b&gt;Database Modeller&lt;/b&gt;&lt;br /&gt;&lt;a href=&quot;http://oxygene.sk/lukas/dbmodel/&quot;&gt;http://oxygene.sk/lukas/dbmodel/&lt;/a&gt;&lt;br /&gt;Copyright (C) 2008 Lukáš Lalinský&lt;/p&gt;
</translation>
    </message>
</context>
<context>
    <name>QtColorPicker</name>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="279"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="405"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="508"/>
        <source>Black</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="406"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="509"/>
        <source>White</source>
        <translation type="unfinished">Biela</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="407"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="510"/>
        <source>Red</source>
        <translation type="unfinished">Červená</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="408"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="511"/>
        <source>Dark red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="409"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="512"/>
        <source>Green</source>
        <translation type="unfinished">Zelená</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="410"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="513"/>
        <source>Dark green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="411"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="514"/>
        <source>Blue</source>
        <translation type="unfinished">Modrá</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="412"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="515"/>
        <source>Dark blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="413"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="516"/>
        <source>Cyan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="414"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="517"/>
        <source>Dark cyan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="415"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="518"/>
        <source>Magenta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="416"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="519"/>
        <source>Dark magenta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="417"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="520"/>
        <source>Yellow</source>
        <translation type="unfinished">Žtlá</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="418"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="521"/>
        <source>Dark yellow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="419"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="522"/>
        <source>Gray</source>
        <translation type="unfinished">Šedá</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="420"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="523"/>
        <source>Dark gray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="421"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="524"/>
        <source>Light gray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="439"/>
        <source>Custom</source>
        <translation type="unfinished">Vlastná</translation>
    </message>
</context>
<context>
    <name>TableProperties</name>
    <message>
        <source>&amp;Definition</source>
        <translation type="obsolete">&amp;Definícia</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="obsolete">Názov:</translation>
    </message>
    <message>
        <source>&amp;Columns</source>
        <translation type="obsolete">Stĺ&amp;ce</translation>
    </message>
    <message>
        <source>Add new column</source>
        <translation type="obsolete">Pridať nový stĺpec</translation>
    </message>
    <message>
        <source>&amp;Add</source>
        <translation type="obsolete">Prid&amp;ať</translation>
    </message>
    <message>
        <source>Remove selected column</source>
        <translation type="obsolete">Odstrániť vybraný stĺpec</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation type="obsolete">Odst&amp;rániť</translation>
    </message>
    <message>
        <source>Move selected column up</source>
        <translation type="obsolete">Posunúť vybraný stĺpec hore</translation>
    </message>
    <message>
        <source>Move &amp;Up</source>
        <translation type="obsolete">Posunúť &amp;hore</translation>
    </message>
    <message>
        <source>Move selected column down</source>
        <translation type="obsolete">Posunúť vybraný stĺpec dole</translation>
    </message>
    <message>
        <source>Move &amp;Down</source>
        <translation type="obsolete">Posunúť &amp;dole</translation>
    </message>
</context>
</TS>
