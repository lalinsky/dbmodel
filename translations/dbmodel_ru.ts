<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru">
<context>
    <name>ColorPickerPopup</name>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="897"/>
        <source>Custom</source>
        <translation>особый</translation>
    </message>
</context>
<context>
    <name>ColumnListModel</name>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="177"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="179"/>
        <source>Data Type</source>
        <translation>Тип данных</translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="181"/>
        <source>Req&apos;d</source>
        <translation>Обязательное</translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="183"/>
        <source>PK</source>
        <translation>Первичный ключ</translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="185"/>
        <source>Notes</source>
        <translation>Примечание</translation>
    </message>
</context>
<context>
    <name>DatabaseRelationshipForm</name>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="16"/>
        <source>Name:</source>
        <translation>Название:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="28"/>
        <source>Automatic</source>
        <translation>Автоматически</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="37"/>
        <source>Cardinality:</source>
        <translation>Тип связи:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="46"/>
        <source>1:1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="53"/>
        <source>1:N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="60"/>
        <source>M:N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="82"/>
        <source>Modality:</source>
        <translation>Обязательность:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="91"/>
        <source>Parent is optional</source>
        <translation>Родитель не обязателен</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="98"/>
        <source>Child is optional</source>
        <translation>Потомок не обязателен</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="120"/>
        <source>Columns:</source>
        <translation>Столбцы:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="139"/>
        <source>references</source>
        <translation>связан с</translation>
    </message>
</context>
<context>
    <name>DatabaseRelationshipProperties</name>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.cpp" line="53"/>
        <source>&amp;Relationship</source>
        <translation>&amp;Связь</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="obsolete">Название:</translation>
    </message>
    <message>
        <source>Cardinality:</source>
        <translation type="obsolete">Тип связи:</translation>
    </message>
</context>
<context>
    <name>DatabaseTableProperties</name>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="50"/>
        <source>&amp;Table</source>
        <translation>&amp;Таблица</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="51"/>
        <source>&amp;Columns</source>
        <translation>&amp;Колонки</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="68"/>
        <source>Name:</source>
        <translation>Название:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="72"/>
        <source>White</source>
        <translation>белый</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="73"/>
        <location filename="../src/items/database/databasetableproperties.cpp" line="74"/>
        <location filename="../src/items/database/databasetableproperties.cpp" line="75"/>
        <source>Gray</source>
        <translation>серый</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="76"/>
        <source>Red</source>
        <translation>красный</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="77"/>
        <source>Brown</source>
        <translation>коричневый</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="78"/>
        <source>Pink</source>
        <translation>розовый</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="79"/>
        <source>Yellow</source>
        <translation>жёлный</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="80"/>
        <source>Green</source>
        <translation>зелёный</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="81"/>
        <source>Blue</source>
        <translation>голубой</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="83"/>
        <source>Color:</source>
        <translation>Цвет:</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="108"/>
        <source>Move &amp;Up</source>
        <translation>Переместить &amp;вверх</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="109"/>
        <source>Move &amp;Down</source>
        <translation>Переместить &amp;вниз</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="106"/>
        <source>&amp;Add</source>
        <translation>&amp;Добавить</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="107"/>
        <source>&amp;Remove</source>
        <translation>&amp;Удалить</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="204"/>
        <source>&amp;New</source>
        <translation>&amp;Создать</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="211"/>
        <source>&amp;Open...</source>
        <translation>&amp;Открыть...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="217"/>
        <source>&amp;Save</source>
        <translation>&amp;Сохранить</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="224"/>
        <source>Save &amp;As...</source>
        <translation>Сохранить &amp;как...</translation>
    </message>
    <message>
        <source>Export...</source>
        <translation type="obsolete">Экспортировать...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="259"/>
        <source>Select</source>
        <translation>Выбор</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="265"/>
        <source>Add new table</source>
        <translation>Добаление новой таблицы</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="270"/>
        <source>Add new relation</source>
        <translation>Добавление новой связи</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="274"/>
        <source>&amp;Undo</source>
        <translation>О&amp;тменить</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="277"/>
        <source>Re&amp;do</source>
        <translation>&amp;Повторить</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="354"/>
        <location filename="../src/mainwindow.cpp" line="403"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="369"/>
        <source>&amp;Mode</source>
        <translation>&amp;Режим</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>50%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>70%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>85%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>100%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>125%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>150%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="320"/>
        <source>&amp;Quit</source>
        <translation>В&amp;ыход</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="304"/>
        <source>&amp;Delete</source>
        <translation>&amp;Удалить</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="461"/>
        <location filename="../src/mainwindow.cpp" line="528"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="461"/>
        <location filename="../src/mainwindow.cpp" line="528"/>
        <source>Unknown format.</source>
        <translation>Неизвестный формат.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="423"/>
        <source>&amp;Edit</source>
        <translation>&amp;Правка</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="233"/>
        <source>E&amp;xport...</source>
        <translation>&amp;Экспортировать...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="237"/>
        <source>Page Set&amp;up...</source>
        <translation>Пара&amp;метры страницы...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="241"/>
        <source>&amp;Print...</source>
        <translation>Пе&amp;чать...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="247"/>
        <source>Print Previe&amp;w...</source>
        <translation>Пре&amp;дпросмотр...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="286"/>
        <source>Cu&amp;t</source>
        <translation>Вы&amp;резать</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="292"/>
        <source>&amp;Copy</source>
        <translation>&amp;Копировать</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="298"/>
        <source>&amp;Paste</source>
        <translation>&amp;Вставить</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="385"/>
        <location filename="../src/mainwindow.cpp" line="433"/>
        <source>&amp;View</source>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="480"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation>Документ был изменён.
Желаете ли вы сохранить изменения?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="741"/>
        <source>&amp;%1. %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="310"/>
        <source>&amp;About...</source>
        <translation>&amp;О программе...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="442"/>
        <source>&amp;Help</source>
        <translation>&amp;Справка</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="798"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="166"/>
        <source>&amp;Properties</source>
        <translation>&amp;Свойства</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="314"/>
        <source>&amp;Close</source>
        <translation>&amp;Закрыть</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="326"/>
        <source>Show &amp;Grid</source>
        <translation>Показывать &amp;сетку</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="798"/>
        <source>&lt;p&gt;
&lt;b&gt;Database Modeller </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="813"/>
        <source>Untitled</source>
        <translation>Безымянный</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="330"/>
        <source>&amp;Notation</source>
        <translation>&amp;Обозначения</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="333"/>
        <source>&amp;Relational</source>
        <translation>&amp;Реляционное</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="337"/>
        <source>&amp;Crow&apos;s Foot</source>
        <translation>&amp;Воронья лапка</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="438"/>
        <source>&amp;Diagram</source>
        <translation>&amp;Схема</translation>
    </message>
    <message>
        <source>&lt;p&gt;
&lt;b&gt;Database Modeller&lt;/b&gt;&lt;br /&gt;
&lt;a href=&quot;http://oxygene.sk/lukas/dbmodel/&quot;&gt;http://oxygene.sk/lukas/dbmodel/&lt;/a&gt;&lt;br /&gt;
Copyright (C) 2008 Lukas Lalinsky
&lt;/p&gt;
</source>
        <translation type="obsolete">&lt;p&gt;&lt;b&gt;Database Modeller&lt;/b&gt;&lt;br /&gt;&lt;a href=&quot;http://oxygene.sk/lukas/dbmodel/&quot;&gt;http://oxygene.sk/lukas/dbmodel/&lt;/a&gt;&lt;br /&gt;Copyright (C) 2008 Lukáš Lalinský&lt;/p&gt;
</translation>
    </message>
</context>
<context>
    <name>QtColorPicker</name>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="279"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="405"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="508"/>
        <source>Black</source>
        <translation>чёрный</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="406"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="509"/>
        <source>White</source>
        <translation>белый</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="407"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="510"/>
        <source>Red</source>
        <translation>красный</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="408"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="511"/>
        <source>Dark red</source>
        <translation>тёмно-красный</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="409"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="512"/>
        <source>Green</source>
        <translation>зелёный</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="410"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="513"/>
        <source>Dark green</source>
        <translation>тёмно-зелёный</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="411"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="514"/>
        <source>Blue</source>
        <translation>голубой</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="412"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="515"/>
        <source>Dark blue</source>
        <translation>синый</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="413"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="516"/>
        <source>Cyan</source>
        <translation>циан</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="414"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="517"/>
        <source>Dark cyan</source>
        <translation>тёмный циан</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="415"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="518"/>
        <source>Magenta</source>
        <translation>пурпурный</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="416"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="519"/>
        <source>Dark magenta</source>
        <translation>тёмно-пурпурный</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="417"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="520"/>
        <source>Yellow</source>
        <translation>жёлный</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="418"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="521"/>
        <source>Dark yellow</source>
        <translation>тёмно-жёлтый</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="419"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="522"/>
        <source>Gray</source>
        <translation>серый</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="420"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="523"/>
        <source>Dark gray</source>
        <translation>тёмно-серый</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="421"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="524"/>
        <source>Light gray</source>
        <translation>светло-серый</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="439"/>
        <source>Custom</source>
        <translation>особый</translation>
    </message>
</context>
<context>
    <name>TableProperties</name>
    <message>
        <source>&amp;Definition</source>
        <translation type="obsolete">&amp;Определение</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="obsolete">Название:</translation>
    </message>
    <message>
        <source>&amp;Columns</source>
        <translation type="obsolete">&amp;Столбцы</translation>
    </message>
    <message>
        <source>Add new column</source>
        <translation type="obsolete">Добавить новый стлбец</translation>
    </message>
    <message>
        <source>&amp;Add</source>
        <translation type="obsolete">&amp;Добавить</translation>
    </message>
    <message>
        <source>Remove selected column</source>
        <translation type="obsolete">Удалить выбранный столбец</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation type="obsolete">&amp;Удалить</translation>
    </message>
    <message>
        <source>Move selected column up</source>
        <translation type="obsolete">Передвинуть выбранный столбец выше</translation>
    </message>
    <message>
        <source>Move &amp;Up</source>
        <translation type="obsolete">Переместить &amp;выше</translation>
    </message>
    <message>
        <source>Move selected column down</source>
        <translation type="obsolete">Передвинуть выбранный столбец ниже</translation>
    </message>
    <message>
        <source>Move &amp;Down</source>
        <translation type="obsolete">Переместить &amp;ниже</translation>
    </message>
</context>
</TS>
