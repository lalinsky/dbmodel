TEMPLATE = subdirs
CONFIG += ordered
SUBDIRS = src

isEmpty(QMAKE_LUPDATE) {
    win32:QMAKE_LUPDATE = $$[QT_INSTALL_BINS]\lupdate.exe
    else:QMAKE_LUPDATE = $$[QT_INSTALL_BINS]/lupdate
}

lupdate.commands = $$QMAKE_LUPDATE dbmodel.pro
QMAKE_EXTRA_TARGETS += lupdate
